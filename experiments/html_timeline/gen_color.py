class ColorHasher(object):
    def __init__(self):
        self.color_pallete = [
            "#F44336",
            "#E91E63",
            "#9C27B0",
            "#673AB7",
            "#3F51B5",
            "#009688",
            "#2196F3",
            "#00BCD4",
            "#4CAF50",
            "#8BC34A",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#795548"
        ]
        self.hashes = {}
        self._rev_hash = {}

    def hashval(self, str):
        hash = 0
        # Take ordinal number of char in str, and just add
        for x in str:
            hash += (ord(x))
        return hash % len(self.color_pallete)  # Depending on the range, do a modulo operation

    def gen_color_for(self, key):
        # indexed entry
        if key in self.hashes:
            return self.hashes[key]
        index = self.hashval(key)
        gen_color = self.color_pallete[index]
        for i in range(1, len(self.color_pallete)):
            if gen_color not in self._rev_hash:
                break
            gen_color = self.color_pallete[(index + i) % len(self.color_pallete)]

        # set index_entry
        self.hashes[key] = gen_color
        self._rev_hash[gen_color] = key
        return gen_color
