#!/usr/bin/env python
import os

from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from flask_security import Security, SQLAlchemyUserDatastore
from flaskext.markdown import Markdown
from inflector import English

from utils import camel_case_to_train_case

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['SECURITY_PASSWORD_SALT'] = 'super-secret'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_CHANGEABLE'] = True
app.config['SECURITY_SEND_PASSWORD_CHANGE_EMAIL'] = False

# override for prod
if os.environ.get("FLASK_WEBAPP_ENV") == "prod":
    import sensitive

    if sensitive.SECRET_KEY == "CHANGEME" \
            or sensitive.SECURITY_PASSWORD_SALT == "CHANGEME":
        raise Exception("Default security key detected, generate more secure ones.")
    app.config['DEBUG'] = False
    app.config['SECRET_KEY'] = sensitive.SECRET_KEY
    app.config['SECURITY_PASSWORD_SALT'] = sensitive.SECURITY_PASSWORD_SALT

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['WHOOSHEE_DIR'] = os.path.join(os.getcwd(), 'search_index')

app.config['DEBUG_TB_ENABLED'] = False
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

# Initialize other plugins
Markdown(app, auto_escape=True)
DebugToolbarExtension(app)


# Register views
def register_views():
    from csrf_protect import csrf
    csrf.init_app(app)

    from store import db, migrate
    db.init_app(app)

    import models
    migrate.init_app(app, db)

    from search import whooshee
    whooshee.init_app(app)

    from auth_models import Role, User
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    Security(app, user_datastore)

    from jinja_filters import register_filters
    register_filters(app)

    from views import client_bp
    app.register_blueprint(client_bp)

    from views import delete_instance_view_func
    for model in [models.InboxEntry, models.Task, models.CalendarEvent, models.Note, models.Log,
                  models.Goal, models.Tracker, ]:
        model_name = English().pluralize(camel_case_to_train_case(model.__name__))
        url = "/{}/<int:instance_id>/delete".format(model_name)
        app.add_url_rule(url, endpoint='{name}_delete'.format(name=model_name),
                         view_func=delete_instance_view_func(model))


register_views()

if __name__ == '__main__':
    app.run(host="0.0.0.0")
