from __future__ import division

from collections import defaultdict
from copy import copy, deepcopy
from math import ceil

from datetime import datetime, timedelta
from dateutil import tz
from flask import request

from constants import DEFAULT_TRACKER_SESSIONS_SUM_BY
from models import TrackerSession, Activity, Tracker, ActivityComponent


def parse_ts(ts_name, all_day=False):
    ts_date = request.form.get('{}_date'.format(ts_name))
    if all_day:
        ts_time = '23:59'
    else:
        ts_time = request.form.get('{}_time'.format(ts_name))
    try:
        dt = datetime.strptime('{}T{}'.format(ts_date, ts_time), '%Y-%m-%dT%H:%M')
        if all_day:
            dt = dt.replace(second=59, microsecond=999999)
        return dt
    except Exception as e:
        raise Exception("Error parsing {}. Details: {}".format(ts_name, e))


def put_in_crates_of_time(instances, crate_length):
    earliest_time = instances[-1].started_ts
    latest_time = instances[0].finished_ts or datetime.utcnow()
    crates, crate = [], []
    crate_new, crate_last = True, False
    crate_latest = None
    crate_earliest = latest_time
    for instance in instances:
        if crate_new:
            if crate:
                crates.append({
                    'start': crate_earliest,
                    'end': crate_latest,
                    'crate': crate,
                })
            if crate_last:
                break
            crate = []
            crate_latest = crate_earliest
            crate_earliest = crate_latest - timedelta(minutes=crate_length)
            if crate_earliest < earliest_time:
                crate_last = True
            crate_new = False
        if instance.started_ts < crate_earliest:
            crate_new = True
        if instance.finished_ts and instance.finished_ts <= crate_latest and crate_earliest <= instance.started_ts <= crate_latest:
            crate.append(instance)
    if crate:
        crates.append({
            'start': crate_earliest,
            'end': crate_latest,
            'crate': crate,
        })
    return crates


def sum_activity_sessions(activity_sessions, sum_by_minutes=DEFAULT_TRACKER_SESSIONS_SUM_BY):
    summed_activity_sessions = []
    if activity_sessions:

        crates = put_in_crates_of_time(activity_sessions, sum_by_minutes)

        for crate_details in crates:
            crate_summary = defaultdict(lambda: dict(total_time=0, instances=0))
            for activity_session in crate_details['crate']:
                if activity_session.finished_ts:
                    crate_summary[activity_session.activity.name]['instances'] += 1
                    crate_summary[activity_session.activity.name]['total_time'] += (activity_session.finished_ts -
                                                                                    activity_session.started_ts).seconds

            summed_activity_session = {
                'started_ts': crate_details['start'],
                'finished_ts': crate_details['end'],
                'total_time': 0,
                'activity_sessions': []
            }
            for name, values in crate_summary.iteritems():
                this_total_time = values['total_time']
                summed_activity_session['activity_sessions'].append({
                    "name": name,
                    "total_time": this_total_time,
                })
                summed_activity_session['total_time'] += this_total_time
            summed_activity_session['activity_sessions'] = sorted(summed_activity_session['activity_sessions'],
                                                                  key=lambda x: x['total_time'], reverse=True)
            summed_activity_sessions.append(summed_activity_session)
    return summed_activity_sessions


def sum_tracker_sessions(tracker_sessions, sum_by_minutes=DEFAULT_TRACKER_SESSIONS_SUM_BY):
    summed_tracker_sessions = []
    if tracker_sessions:

        crates = put_in_crates_of_time(tracker_sessions, sum_by_minutes)

        for crate_details in crates:
            crate_summary = defaultdict(lambda: dict(active_time=0, total_time=0))
            for tracker_session in crate_details['crate']:
                crate_summary[tracker_session.key]['active_time'] += tracker_session.active_time
                crate_summary[tracker_session.key]['total_time'] += tracker_session.total_time

            summed_tracker_session = {
                'started_ts': crate_details['start'],
                'finished_ts': crate_details['end'],
                'tracker_sessions': []
            }
            for key, values in crate_summary.iteritems():
                summed_tracker_session['tracker_sessions'].append({
                    "key": key,
                    "active_time": values['active_time'],
                    "total_time": values['total_time'],
                })
            summed_tracker_sessions.append(summed_tracker_session)
    return summed_tracker_sessions


def get_user_tz(user):
    return tz.tzoffset(None, timedelta(seconds=user.tz_offset_seconds))


def get_now_user(user):
    """
    Gets the user specific time based on his timezone
    """
    tzinfo = get_user_tz(user)
    return datetime.utcnow().replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def get_today_user(user):
    """
    Gets the user specific beginning of today based on his timezone
    """
    now = get_now_user(user)
    return datetime(year=now.year, month=now.month, day=now.day, tzinfo=now.tzinfo)


def get_tomorrow_user(user):
    """
    Gets the user specific beginning of tomorrow based on his timezone
    """
    today = get_today_user(user)
    return today + timedelta(days=1)


def generate_timeline(instances, name=''):
    earliest_time, latest_time = datetime.utcnow(), datetime.utcnow()
    if instances:
        earliest_time = instances[0].started_ts
        latest_time = instances[-1].finished_ts
    timeline_data = dict(
        name=name,
        started_ts=earliest_time,
        finished_ts=latest_time,
        total_time=(latest_time - earliest_time).seconds,
        entries=[]
    )
    prev_key = ''
    for tracker_session in instances:
        this_key = tracker_session.key
        if prev_key == this_key and tracker_session.active_time:
            this_seconds = (tracker_session.finished_ts - tracker_session.started_ts).seconds
            timeline_data['entries'][-1]['duration'] += this_seconds
            timeline_data['entries'][-1]['total_time'] += tracker_session.total_time
            timeline_data['entries'][-1]['active_time'] += tracker_session.active_time
            timeline_data['entries'][-1]['finished_ts'] = tracker_session.finished_ts
        else:
            entry = dict(
                key=tracker_session.key,
                started_ts=tracker_session.started_ts,
                finished_ts=tracker_session.finished_ts,
                start=(tracker_session.started_ts - earliest_time).seconds,
                duration=(tracker_session.finished_ts - tracker_session.started_ts).seconds,
                total_time=tracker_session.total_time,
                active_time=tracker_session.active_time,
            )
            timeline_data['entries'].append(entry)
        prev_key = this_key
    return timeline_data


def to_ts(t):
    return int(t.strftime("%s"))


def summarize_activities(user, start_ts, finish_ts, resolution=1):
    tdelta = (finish_ts - start_ts)
    tdelta_seconds = tdelta.days * 86400 + tdelta.seconds

    timeline_template = {
        'name': '',
        'entries': []
    }

    activities_summary_template = {
        'activity_name': '',
        'total_time': 0,
        'active_time': 0,
        'total_time_res': [0] * (int(ceil(tdelta_seconds / resolution))),
        'active_time_res': [0] * (int(ceil(tdelta_seconds / resolution))),
        'total_time_bitmap': [0] * (int(ceil(tdelta_seconds))),
        'active_time_bitmap': [0] * (int(ceil(tdelta_seconds))),
        'timelines': []
    }

    start_ts_s = to_ts(start_ts)

    activity_components = ActivityComponent.query.join(Activity).filter(ActivityComponent.user == user).all()
    tracker_types = [ac.tracker_type for ac in activity_components]
    tracker_sessions = TrackerSession.query.join(Tracker). \
        filter(TrackerSession.user == user,
               Tracker.tracker_type.in_(tracker_types),
               TrackerSession.get_filter_in_bounds(start_ts, finish_ts)).all()

    activities_summaries = defaultdict(lambda: deepcopy(activities_summary_template))

    for activity_component in activity_components:
        timeline = copy(timeline_template)
        timeline['name'] = "{} - {}".format(activity_component.tracker_type, activity_component.search_key)
        activity = activity_component.activity
        search_key = activity_component.search_key
        sessions = []
        for tracker_session in tracker_sessions:
            if search_key == tracker_session.key:
                for iter, i in enumerate(range(to_ts(tracker_session.started_ts) - start_ts_s,
                                               to_ts(tracker_session.finished_ts) - start_ts_s)):
                    activities_summaries[activity.name]['total_time_bitmap'][i] += 1
                    activities_summaries[activity.name]['total_time_res'][i // resolution] += 1
                    if iter < tracker_session.active_time:
                        activities_summaries[activity.name]['active_time_bitmap'][i] += 1
                        activities_summaries[activity.name]['active_time_res'][i // resolution] += 1
                sessions.append(tracker_session.serialize())
        timeline['entries'] = sessions

        activities_summaries[activity.name]['activity_name'] = activity.name
        activities_summaries[activity.name]['timelines'].append(timeline)

    for k, v in activities_summaries.iteritems():
        activities_summaries[k]['total_time'] = sum(1 if i else 0 for i in v['total_time_bitmap'])
        activities_summaries[k]['active_time'] = sum(1 if i else 0 for i in v['active_time_bitmap'])
        del activities_summaries[k]['total_time_bitmap']
        del activities_summaries[k]['active_time_bitmap']

    return dict(
        start_ts=start_ts.strftime("%Y-%m-%dT%H:%M:%S"),
        finish_ts=finish_ts.strftime("%Y-%m-%dT%H:%M:%S"),
        resolution=resolution,
        activities_summaries=activities_summaries
    )
