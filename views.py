import calendar
import json

from datetime import datetime, timedelta
from dateutil import parser as dt_parser
from flask import flash, redirect, request, url_for, render_template, Blueprint, jsonify
from flask_login import current_user, login_required
from sqlalchemy import or_, and_

from constants import DEFAULT_TRACKER_SESSIONS_SUM_BY
from csrf_protect import csrf
from models import InboxEntry, Activity, Task, CalendarEvent, attempt_inbox_entry_process, Note, calendar_event_update, \
    ActivitySession, activity_session_update, Project, Label, TaskLabel, Log, Goal, Tracker, TrackerSession, TrackerEvent, Settings
from store import db
from utils import OrderedDefaultDict, generate_random_string
from views_utils import parse_ts, sum_tracker_sessions, \
    sum_activity_sessions, generate_timeline, summarize_activities, get_today_user, get_tomorrow_user

client_bp = Blueprint('client_bp', __name__)


class AccessDenied(Exception):
    pass

def get_instance_by_id(model, instance_id):
    instance = model.query.filter_by(id=instance_id, user=current_user).first()
    if not instance:
        raise ("{} doesn't exist".format(model.__name__))
    return instance


def delete_instance_view_func(model):
    @login_required
    def delete_instance(instance_id):
        try:
            instance = get_instance_by_id(model, instance_id)
            db.session.delete(instance)
            now = datetime.utcnow()
            log = Log(user=current_user,
                      created_ts=now,
                      body="[{}|{}|deleted]".format(instance.__class__.__name__, instance.id),
                      created_type=Log.CREATED_TYPE_AUTOMATIC,
                      )
            db.session.add(log)
            db.session.commit()
        except Exception as e:
            flash(e, category="error")
            return redirect(request.referrer)
        flash("{} deleted.".format(model.__name__))
        return redirect(request.referrer)

    return delete_instance


class Context(object):
    def __init__(self):
        self.now = None
        self.month_calendar = None
        self.weeknum = None
        self.beginning_of_date = None
        self.end_of_date = None
        self.user_settings = None

    def get_or_create_user_settings(self):
        user_settings = Settings.query.filter_by(user=current_user).first()
        if not user_settings:
            user_settings = Settings.create(user=current_user)
            db.session.add(user_settings)
            db.session.commit()
        return user_settings

    def process_date_filter_params(self, default_start=None, default_end=None):
        date_filter = request.args.get('date_filter')
        date_range_filter = request.args.get('date_range_filter')
        datetime_range_filter = request.args.get('datetime_range_filter')

        if date_filter:
            try:
                parsed_date = datetime.strptime(date_filter, "%Y-%m-%d")
            except ValueError:
                raise Exception("Incorrect date format.")
            beginning_of_date = parsed_date.replace(hour=0, minute=0, second=0)
            end_of_date = (parsed_date + timedelta(days=1)).replace(hour=0, minute=0, second=0)
        elif date_range_filter:
            try:
                start_date, end_date = date_range_filter.split('--')
            except ValueError:
                raise Exception("Incorrect date range format.")
            try:
                parsed_start_date = datetime.strptime(start_date, "%Y-%m-%d")
                parsed_end_date = datetime.strptime(end_date, "%Y-%m-%d")
            except ValueError:
                raise Exception("Incorrect date format.")
            beginning_of_date = parsed_start_date.replace(hour=0, minute=0, second=0)
            end_of_date = (parsed_end_date + timedelta(days=1)).replace(hour=0, minute=0, second=0)
        elif datetime_range_filter:
            try:
                start_date, end_date = datetime_range_filter.split('--')
            except ValueError:
                raise Exception("Incorrect date range format.")
            try:
                beginning_of_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S")
                end_of_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S")
            except ValueError:
                raise Exception("Incorrect date format.")
        else:
            # get today by default
            beginning_of_date = default_start or get_today_user(current_user)
            end_of_date = default_end or get_tomorrow_user(current_user)
        if beginning_of_date > end_of_date:
            raise Exception("Date range start date is after end date.")

        return beginning_of_date, end_of_date

    def build_context(self, model):
        self.user_settings = self.get_or_create_user_settings()
        model_enabled = getattr(self.user_settings, "{}_enabled".format(model))
        if not model_enabled:
            raise AccessDenied("Access denied motherfucker. Go to your settings and ask politely for {}".format(model))
        selected_date = request.args.get('selected_date')
        if selected_date:
            try:
                self.now = datetime.strptime(selected_date, '%Y-%m-%d')
            except ValueError:
                raise Exception("Incorrect date format")
        else:
            self.now = datetime.utcnow()
        self.beginning_of_date, self.end_of_date = self.process_date_filter_params()
        self.month_calendar, self.weeknum = get_weekdays(self.now)
        return self


@client_bp.route('/')
def home():
    if current_user.is_authenticated:
        return redirect(url_for('client_bp.get_inbox_entries'))
    return redirect(url_for('security.register'))


@client_bp.route('/inbox-entries')
@login_required
def get_inbox_entries():
    try:
        ctx = Context().build_context("inbox_entries")
    except Exception as e:
        flash(e.message, category="error")
        return render_template("server_error.html")



    inbox_entries = InboxEntry.query. \
        filter_by(user=current_user, is_processed=False). \
        order_by(InboxEntry.created_ts.desc()).all()
    dated_inbox_entries = OrderedDefaultDict(list)
    for inbox_entry in inbox_entries:
        dated_inbox_entries[inbox_entry.created_ts.date()].append(inbox_entry)

    running_activities = Activity.query.filter(Activity.user == current_user,
                                               Activity.running_session != None).all()

    task_date_filters = {}
    if ctx.beginning_of_date and ctx.end_of_date:
        task_date_filters = Task.get_filter_in_bounds(ctx.beginning_of_date, ctx.end_of_date)
    tasks = Task.query. \
        filter(or_(Task.user == current_user,
                   and_(Task.user == current_user,
                        task_date_filters))). \
        order_by(Task.created_ts.desc()).all()
    not_done_tasks = set([t for t in tasks if not t.is_done])
    done_tasks = set([t for t in tasks if t.is_done])

    calendar_event_date_filters = {}
    if ctx.beginning_of_date and ctx.end_of_date:
        calendar_event_date_filters = CalendarEvent.get_filter_in_bounds(ctx.beginning_of_date,
                                                                         ctx.end_of_date)
    calendar_events = CalendarEvent.query \
        .filter_by(user=current_user) \
        .filter(calendar_event_date_filters) \
        .order_by(CalendarEvent.to_start_ts).all()
    dated_calendar_events = OrderedDefaultDict(list)
    for calendar_event in calendar_events:
        dated_calendar_events[calendar_event.to_start_ts.date()].append(calendar_event)

    inbox_entry_value = request.args.get('inbox_entry_value')
    if inbox_entry_value not in ['task:', 'event:', 'start:', ]:
        inbox_entry_value = None

    return render_template('inbox-entries.html',
                           inbox_entry_value=inbox_entry_value,
                           running_activities=running_activities,
                           not_done_tasks=not_done_tasks,
                           done_tasks=done_tasks,
                           dated_calendar_events=dated_calendar_events,
                           dated_inbox_entries=dated_inbox_entries,
                           ctx=ctx
                           )


@client_bp.route('/inbox-entries/create', methods=['POST'])
@login_required
def create_inbox_entry():
    body = request.form.get('body')
    is_processed, processed_type, processed_instance_id = None, None, None
    try:
        is_processed, processed_type, processed_instance_id = attempt_inbox_entry_process(user=current_user, body=body)
    except Exception as e:
        flash(e.message, category="error")
    inbox_entry = InboxEntry(user=current_user,
                             body=body,
                             is_processed=is_processed,
                             processed_type=processed_type,
                             processed_instance_id=processed_instance_id,
                             created_ts=datetime.utcnow())
    db.session.add(inbox_entry)
    db.session.commit()
    flash("Inbox Entry created.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_inbox_entry(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('edit-inbox-entry.html', inbox_entry=inbox_entry)
    body = request.form.get('body')
    inbox_entry.body = body
    db.session.add(inbox_entry)
    db.session.commit()
    flash("Inbox Entry edited.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/transform/task')
@login_required
def trasform_inbox_entry_task(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)

    now = datetime.utcnow()
    task = Task(title=inbox_entry.body, user=current_user, created_ts=now)
    db.session.add(task)
    db.session.commit()

    inbox_entry.is_processed = True
    inbox_entry.processed_type = "task"
    inbox_entry.processed_instance_id = task.id
    db.session.add(inbox_entry)
    db.session.commit()

    flash("Inbox Entry transformed to Task.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/transform/calendar-event')
@login_required
def trasform_inbox_entry_calendar_event(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)

    now = datetime.utcnow()
    start_ts = datetime.utcnow().replace(microsecond=0, second=0, minute=0) + timedelta(hours=1)
    end_ts = start_ts + timedelta(hours=1)
    calendar_event = CalendarEvent(title=inbox_entry.body, user=current_user,
                                   to_start_ts=start_ts,
                                   to_end_ts=end_ts,
                                   created_ts=now)
    db.session.add(calendar_event)
    db.session.commit()

    inbox_entry.is_processed = True
    inbox_entry.processed_type = "calendar_event"
    inbox_entry.processed_instance_id = calendar_event.id
    db.session.add(inbox_entry)
    db.session.commit()

    flash("Inbox Entry transformed to Calendar Event with default start/end time.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/transform/note')
@login_required
def trasform_inbox_entry_note(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)
    db.session.add(inbox_entry)

    now = datetime.utcnow()
    note = Note(body=inbox_entry.body, user=current_user, created_ts=now)
    db.session.add(note)
    db.session.commit()

    inbox_entry.is_processed = True
    inbox_entry.processed_type = "note"
    inbox_entry.processed_instance_id = note.id
    db.session.add(inbox_entry)
    db.session.commit()

    flash("Inbox Entry transformed to Note.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/transform/log')
@login_required
def trasform_inbox_entry_log(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)
    db.session.add(inbox_entry)

    now = datetime.utcnow()
    log = Log(body=inbox_entry.body, user=current_user, created_ts=now)
    db.session.add(log)
    db.session.commit()

    inbox_entry.is_processed = True
    inbox_entry.processed_type = "log"
    inbox_entry.processed_instance_id = log.id
    db.session.add(inbox_entry)
    db.session.commit()

    flash("Inbox Entry transformed to Log.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/process')
@login_required
def process_inbox_entry(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)
    inbox_entry.is_processed = not inbox_entry.is_processed
    db.session.add(inbox_entry)
    db.session.commit()
    flash("Inbox Entry processed.")
    return redirect(request.referrer)


def get_weekdays(now):
    prev_month = now.replace(day=1) - timedelta(days=1)
    next_month = now.replace(day=28) + timedelta(days=7)
    cal_inst = calendar.Calendar()
    month_days = [(a, now.month, now.year, 'today' if a == now.day else 'cur') for a in
                  cal_inst.itermonthdays(now.year, now.month)]
    next_month_days = [(a, next_month.month, next_month.year, 'next') for a in
                       cal_inst.itermonthdays(next_month.year, next_month.month)]
    prev_month_days = [(a, prev_month.month, prev_month.year, 'prev') for a in
                       cal_inst.itermonthdays(prev_month.year, prev_month.month)]
    month_days[:7] = [a if a[0] != 0 else prev_month_days[-7 + i] for i, a in enumerate(month_days[:7])]
    month_days[-7:] = [a if a[0] != 0 else next_month_days[i] for i, a in enumerate(month_days[-7:])]
    today_day_index = month_days.index((now.day, now.month, now.year, 'today'))
    return [month_days[i:i + 7] for i in range(0, len(month_days), 7)], today_day_index / 7


@client_bp.route('/calendar-events')
@login_required
def get_calendar_events():
    try:
        ctx = Context().build_context("calendar_events")
    except AccessDenied as ae:
        flash(ae.message, category="error")
        return redirect(url_for("client_bp.home"))
    except Exception as e:
        flash(e.message, category="error")
        return render_template("server_error.html")

    calendar_events = CalendarEvent.get_events_in_bounds(user=current_user,
                                                         start=ctx.beginning_of_date,
                                                         end=ctx.end_of_date)
    calendar_events = sorted(calendar_events, key=lambda x: x.to_start_ts)
    dated_calendar_events = OrderedDefaultDict(list)
    for calendar_event in calendar_events:
        dated_calendar_events[calendar_event.to_start_ts.date()].append(calendar_event)

    default_start_event = datetime.utcnow().replace(microsecond=0, second=0, minute=0) + timedelta(hours=1)
    default_end_event = default_start_event + timedelta(hours=1)

    default_end_recurring = datetime.utcnow() + timedelta(days=30)

    recurring_types = CalendarEvent.RECURRING_TYPES

    return render_template('calendar-events.html',
                           default_start_event=default_start_event,
                           default_end_event=default_end_event,
                           dated_calendar_events=dated_calendar_events,
                           default_end_recurring=default_end_recurring,
                           recurring_types=recurring_types,
                           ctx=ctx,
                           )


@client_bp.route('/calendar-events/create', methods=['POST'])
@login_required
def create_calendar_event():
    calendar_event = CalendarEvent.create(log_class=Log,
                                          user=current_user,
                                          )
    title = request.form.get('title')
    calendar_event.title = title
    all_day = bool(request.form.get('all_day', False))
    calendar_event.all_day = all_day

    try:
        to_start_ts = parse_ts('to_start_ts', all_day=all_day)
        calendar_event.to_start_ts = to_start_ts
    except Exception as e:
        flash("Error: {}".format(e), category="error")
        return redirect(request.referrer)
    try:
        to_finish_ts = parse_ts('to_finish_ts', all_day=all_day)
    except Exception as e:
        flash("Error: {}".format(e), category="error")
        return redirect(request.referrer)
    if to_finish_ts < calendar_event.to_start_ts:
        flash("End time can't be before start time.", category="error")
        return redirect(request.referrer)
    calendar_event.to_finish_ts = to_finish_ts

    recurring_type = request.form.get('recurring_type')
    if not recurring_type == CalendarEvent.RECURRING_NONE:
        # processing recurring event
        calendar_event.recurring_to_start_ts = to_start_ts
        calendar_event.recurring_to_finish_ts = to_finish_ts

        recurring_every = request.form.get('recurring_every')

        calendar_event.recurring_type = recurring_type
        calendar_event.recurring_every = recurring_every

        if recurring_type == CalendarEvent.RECURRING_WEEKLY:
            weekly_mask = ''.join([str(int(bool(request.form.get('weekly_{}'.format(day), False))))
                                   for day in range(1, 8)])
            calendar_event.recurring_weekly_mask = weekly_mask

        recurring_ends_type = request.form.get('recurring_ends_type')
        calendar_event.recurring_ends_type = recurring_ends_type

        if recurring_ends_type == CalendarEvent.RECURRING_ENDS_AFTER_N:
            recurring_ends_after_x = request.form.get('recurring_ends_after_x')
            calendar_event.recurring_ends_after_x = recurring_ends_after_x
        elif recurring_ends_type == CalendarEvent.RECURRING_ENDS_ON:
            recurring_ends_on = request.form.get('recurring_ends_on')
            if recurring_ends_on < to_start_ts:
                flash("Recurring can't end before before start time.", category="error")
                return redirect(request.referrer)
            calendar_event.recurring_ends_on = recurring_ends_on

    now = datetime.utcnow()
    calendar_event.created_ts = now
    db.session.add(calendar_event)
    db.session.commit()
    flash("Calendar Event created.")
    return redirect(request.referrer)


@client_bp.route('/calendar-events/<int:calendar_event_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_calendar_event(calendar_event_id):
    try:
        calendar_event = CalendarEvent.get_by(id=calendar_event_id, user=current_user)
        if request.method == 'GET':
            return render_template('edit-calendar-event.html', calendar_event=calendar_event)
        try:
            to_finish_ts = parse_ts('to_finish_ts')
        except Exception:
            to_finish_ts = None
        calendar_event_update(calendar_event,
                              title=request.form.get('title'),
                              to_start_ts=parse_ts('to_start_ts'),
                              to_finish_ts=to_finish_ts)
    except Exception as e:
        flash("Error: {}.".format(e.message), category="error")
        return redirect(request.referrer)
    flash("Calendar Event edited.")
    return redirect(request.referrer)


@client_bp.route('/tasks')
@login_required
def get_tasks():
    try:
        ctx = Context().build_context()
    except Exception as e:
        flash(e.message, category="error")
        return redirect(url_for('client_bp.home'))

    filters = []
    selected_project_id = request.args.get('project_id')
    if selected_project_id:
        selected_project = Project.query.filter_by(user=current_user, id=selected_project_id).first()
        if selected_project:
            filters.append(Task.project_id == selected_project_id)
    tasks = Task.query. \
        filter_by(user=current_user). \
        filter(*filters). \
        order_by(Task.created_ts.desc()).all()
    projects = Project.query.filter_by(user=current_user).all()
    labels = Label.query.filter_by(user=current_user).all()

    default_start_task = datetime.utcnow()
    default_end_task = datetime.utcnow() + timedelta(days=1)

    return render_template('tasks.html',
                           selected_project_id=selected_project_id,
                           projects=projects,
                           labels=labels,
                           not_done_tasks=[t for t in tasks if not t.is_done],
                           done_tasks=[t for t in tasks if t.is_done],
                           default_start_task=default_start_task,
                           default_end_task=default_end_task,
                           ctx=ctx,
                           )


@client_bp.route('/tasks/create', methods=['POST'])
@login_required
def create_task():
    title = request.form.get('title')

    project_id = request.form.get('project_id')
    project = None
    if project_id:
        existing_project = Project.query.filter_by(user=current_user, id=project_id).first()
        if existing_project:
            project = existing_project

    task = Task.create(log_class=Log,
                       user=current_user,
                       title=title,
                       project=project,
                       )

    labels_request = request.form.get('labels')
    task_labels = []
    if labels_request:
        labels_splitted = labels_request.split(',')
        for label_splitted in labels_splitted:
            label_name = label_splitted.strip()
            existing_label = Label.query.filter_by(name=label_name, user=current_user).first()
            if not existing_label:
                existing_label = Label(name=label_name, user=current_user)
            task_labels.append(existing_label)
    for label in task_labels:
        task_label = TaskLabel(task=task, label=label)
        db.session.add(task_label)

    db.session.add(task)
    db.session.commit()
    flash("Task created.")
    return redirect(request.referrer)


@client_bp.route('/tasks/<int:task_id>/done', methods=['POST'])
@login_required
def done_task(task_id):
    task = Task.query. \
        filter_by(user=current_user, id=task_id).first()
    if not task:
        flash("Task {} doesn't exist".format(task_id), category="error")
        return redirect(request.referrer)
    task.is_done = not task.is_done
    db.session.add(task)
    db.session.commit()
    flash("Task updated.")
    return redirect(request.referrer)


@client_bp.route('/tasks/<int:task_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_task(task_id):
    task = Task.query.filter_by(id=task_id, user=current_user).first()
    if not task:
        flash("Task doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('edit-task.html', task=task)
    title = request.form.get('title')
    task.title = title
    db.session.add(task)
    db.session.commit()
    flash("Task edited.")
    return redirect(request.referrer)


@client_bp.route('/projects/create', methods=['POST'])
@login_required
def create_project():
    name = request.form.get('name')
    project = Project.create(log_class=Log,
                             user=current_user,
                             name=name,
                             )
    db.session.add(project)
    db.session.commit()
    flash("Project created.")
    return redirect(request.referrer)


@client_bp.route('/activities')
@login_required
def get_activities():
    try:
        ctx = Context().build_context()
    except Exception as e:
        flash(e.message, category="error")
        return redirect(url_for('client_bp.home'))
    activities = Activity.query.filter_by(user=current_user).all()

    default_start_session = datetime.utcnow().replace(microsecond=0, second=0, minute=0) - timedelta(hours=1)
    default_end_session = default_start_session + timedelta(hours=1)

    activity_sessions = ActivitySession.query \
        .filter_by(user=current_user, deleted=False) \
        .filter(ActivitySession.get_filter_in_bounds(ctx.beginning_of_date, ctx.end_of_date)) \
        .order_by(ActivitySession.started_ts.desc()).all()
    summed_activity_sessions = sum_activity_sessions(activity_sessions, 1440)

    start_ts, end_ts = process_date_filter_params(datetime.utcnow() - timedelta(minutes=60), datetime.utcnow())
    try:
        resolution = int(request.args.get('resolution', 900))
    except Exception:
        resolution = 900  # 15 minutes
    if resolution < 1:
        resolution = 900
    activities_summaries = summarize_activities(current_user, start_ts, end_ts, resolution)

    return render_template('activities.html',
                           default_start_session=default_start_session,
                           default_end_session=default_end_session,
                           summed_activity_sessions=summed_activity_sessions,
                           activity_sessions=activity_sessions,
                           activities=activities,
                           not_running_activities=[a for a in activities if not a.is_running],
                           running_activities=[a for a in activities if a.is_running],
                           activities_summaries=activities_summaries,
                           ctx=ctx,
                           )


@client_bp.route('/activities/create', methods=['POST'])
@login_required
def create_activity():
    name = request.form.get('name')
    activity = Activity.create(log_class=Log,
                               user=current_user,
                               name=name
                               )
    db.session.add(activity)
    db.session.commit()
    flash("Activity created.")
    return redirect(request.referrer)


@client_bp.route('/activities/<int:activity_id>/run_toggle', methods=['POST'])
@login_required
def activity_run_toggle(activity_id):
    activity = Activity.query. \
        filter_by(user=current_user, id=activity_id).first()
    if not activity:
        flash("Activity {} doesn't exist".format(activity_id), category="error")
        return redirect(request.referrer)
    message = ""
    try:
        if activity.is_running:
            message = "stopped"
            activity.stop(current_user)
        else:
            message = "started"
            activity.start(current_user)
        db.session.commit()
        flash("Activity {}.".format(message))
    except Exception as e:
        flash("Error: Activity not {}. Details: {}".format(message, e.message), category='error')
    return redirect(request.referrer)


@client_bp.route('/activities/<int:activity_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_activity(activity_id):
    activity = Activity.query.filter_by(id=activity_id, user=current_user).first()
    if not activity:
        flash("Activity doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('edit-activity.html', activity=activity)
    name = request.form.get('name')
    activity.name = name
    db.session.add(activity)
    db.session.commit()
    flash("Activity edited.")
    return redirect(request.referrer)


@client_bp.route('/activities/<int:activity_id>/delete')
@login_required
def delete_activity(activity_id):
    activity = Activity.query.filter_by(id=activity_id, user=current_user).first()
    if not activity:
        flash("Activity doesn't exist.", category="error")
        return redirect(request.referrer)

    acitivity_sessions = ActivitySession.query.filter_by(activity=activity, user=current_user).all()
    for acitivity_session in acitivity_sessions:
        db.session.delete(acitivity_session)

    goals = Goal.query.filter_by(activity=activity, user=current_user).all()
    for goal in goals:
        db.session.delete(goal)

    db.session.commit()
    db.session.delete(activity)
    db.session.commit()
    flash("Activity and all associated Activity Sessions and Goals deleted.")
    return redirect(request.referrer)


@client_bp.route('/activity-sessions/create', methods=['POST'])
@login_required
def create_activity_session():
    activity_session = ActivitySession.create(log_class=Log,
                                              user=current_user,
                                              )
    activity_id = request.form.get('activity_id')
    try:
        activity_id = int(activity_id)
    except Exception as e:
        flash("Error: {}".format(e), category="error")
        return redirect(request.referrer)
    activities = Activity.query.filter_by(user=current_user).all()
    if activity_id not in [a.id for a in activities]:
        flash("Error: Non existing activity with id {}.".format(activity_id), category="error")
        return redirect(request.referrer)
    activity_session.activity_id = activity_id
    try:
        started_ts = parse_ts('started_ts')
        activity_session.started_ts = started_ts
    except Exception as e:
        flash("Error: {}".format(e), category="error")
        return redirect(request.referrer)
    try:
        finished_ts = parse_ts('finished_ts')
    except Exception as e:
        flash("Error: {}".format(e), category="error")
        return redirect(request.referrer)
    if finished_ts < activity_session.started_ts:
        flash("End time can't be before start time.", category="error")
        return redirect(request.referrer)
    activity_session.finished_ts = finished_ts
    db.session.add(activity_session)
    db.session.commit()
    flash("Activity Session created.")
    return redirect(request.referrer)


@client_bp.route('/activity-sessions/<int:activity_session_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_activity_session(activity_session_id):
    try:
        activity_session = ActivitySession.get_by(id=activity_session_id, user=current_user)
        if request.method == 'GET':
            activities = Activity.query.filter_by(user=current_user).all()
            return render_template('edit-activity-session.html',
                                   activities=activities,
                                   activity_session=activity_session)
        activity_session_update(user=current_user,
                                activity_session=activity_session,
                                activity_id=request.form.get('activity_id'),
                                started_ts=parse_ts('started_ts'),
                                finished_ts=parse_ts('finished_ts'))
    except Exception as e:
        flash("Error: {}.".format(e.message), category="error")
        return redirect(request.referrer)
    flash("Activity Session edited.")
    return redirect(request.referrer)


@client_bp.route('/activity-sessions/<int:activity_session_id>/delete')
@login_required
def delete_activity_session(activity_session_id):
    activity_session = ActivitySession.query.filter_by(id=activity_session_id, user=current_user).first()
    if not activity_session:
        flash("Activity Session doesn't exist.", category="error")
        return redirect(request.referrer)
    running_activity = Activity.query.filter_by(running_session=activity_session).first()
    if running_activity:
        running_activity.stop(current_user)
    db.session.delete(activity_session)
    db.session.commit()
    flash("Activity Session deleted.")
    return redirect(request.referrer)


@client_bp.route('/goals')
@login_required
def get_goals():
    try:
        ctx = Context().build_context()
    except Exception as e:
        flash(e.message, category="error")
        return redirect(url_for('client_bp.home'))
    goals = Goal.query. \
        filter_by(user=current_user). \
        order_by(Goal.created_ts.desc()).all()
    activities = Activity.query.filter_by(user=current_user).all()
    periods = Goal.PERIODS
    limits = Goal.LIMITS
    return render_template('goals.html',
                           goals=goals,
                           activities=activities,
                           periods=periods,
                           limits=limits,
                           ctx=ctx
                           )


@client_bp.route('/goals/create', methods=['POST'])
@login_required
def create_goal():
    name = request.form.get('name')
    name = name.strip()
    if not name:
        flash("Error: Name can't be empty", category="error")
        return redirect(request.referrer)

    activity_id = request.form.get('activity_id')
    try:
        activity_id = int(activity_id)
    except Exception as e:
        flash("Error: {}".format(e), category="error")
        return redirect(request.referrer)
    activities = Activity.query.filter_by(user=current_user).all()
    if activity_id not in [a.id for a in activities]:
        flash("Error: Non existing activity with id {}.".format(activity_id), category="error")
        return redirect(request.referrer)

    period = request.form.get('period')
    if period not in [p[0] for p in Goal.PERIODS]:
        flash("Error: Invalid period.", category="error")
        return redirect(request.referrer)

    limit = request.form.get('limit')
    if limit not in [l[0] for l in Goal.LIMITS]:
        flash("Error: Invalid limit.", category="error")
        return redirect(request.referrer)

    frequency = request.form.get('frequency')
    if not frequency.isdigit():
        flash("Error: Frequency needs to be a digit.", category="error")
        return redirect(request.referrer)
    frequency = int(frequency)
    if frequency < 1:
        flash("Error: Frequency can't be negative.", category="error")
        return redirect(request.referrer)

    active_minutes = request.form.get('active_minutes')
    if not active_minutes.isdigit():
        flash("Error: Active minutes needs to be a digit.", category="error")
        return redirect(request.referrer)
    active_minutes = int(active_minutes)
    if active_minutes < 1:
        flash("Error: Active minutes can't be negative.", category="error")
        return redirect(request.referrer)

    active_for_period = request.form.get('active-for-period')
    if active_for_period == 'hours':
        active_minutes = active_minutes * 60

    goal = Goal.create(log_class=Log,
                       user=current_user,
                       name=name,
                       activity_id=activity_id,
                       period=period,
                       limit=limit,
                       frequency=frequency,
                       active_minutes=active_minutes,
                       )
    db.session.add(goal)
    db.session.commit()
    flash("Goal created.")
    return redirect(request.referrer)


@client_bp.route('/goals/<goal_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_goal(goal_id):
    goal = Goal.query.filter_by(id=goal_id, user=current_user).first()
    if not goal:
        flash("Goal doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('edit-goal.html', goal=goal)
    body = request.form.get('body')
    goal.body = body
    db.session.add(goal)
    db.session.commit()
    flash("Goal edited.")
    return redirect(request.referrer)


@client_bp.route('/trackers')
@login_required
def get_trackers():
    try:
        ctx = Context().build_context()
    except Exception as e:
        flash(e.message, category="error")
        return redirect(url_for('client_bp.home'))
    trackers = Tracker.query. \
        filter_by(user=current_user). \
        order_by(Tracker.created_ts.desc()).all()

    start_ts, end_ts = process_date_filter_params(datetime.utcnow() - timedelta(minutes=60), datetime.utcnow())
    timelines = dict(
        started_ts=start_ts,
        finished_ts=end_ts,
        timelines=[]
    )
    for tracker in trackers:
        tracker_sessions = TrackerSession.query. \
            filter(TrackerSession.user == current_user,
                   TrackerSession.tracker == tracker,
                   TrackerSession.get_filter_in_bounds(start_ts, end_ts)). \
            order_by(TrackerSession.started_ts).all()
        timeline = generate_timeline(tracker_sessions, name=tracker.name)
        timelines['timelines'].append(timeline)

    tracker_types = Tracker.TRACKER_TYPES
    activities = Activity.query.filter_by(user=current_user).all()
    return render_template('trackers.html',
                           timelines=timelines,
                           activities=activities,
                           trackers=trackers,
                           tracker_types=tracker_types,
                           ctx=ctx
                           )


@client_bp.route('/trackers/<int:tracker_id>')
@login_required
def get_tracker(tracker_id):
    tracker = Tracker.query.filter_by(id=tracker_id, user=current_user).first()
    if not tracker:
        flash("Tracker doesn't exist.", category="error")
        return redirect(request.referrer)

    tracker_sessions = TrackerSession.query.filter_by(user=current_user, tracker=tracker). \
        order_by(TrackerSession.started_ts.desc()).all()

    sum_by_minutes = request.args.get('sum_by_minutes', str(DEFAULT_TRACKER_SESSIONS_SUM_BY))
    if sum_by_minutes:
        if not sum_by_minutes.isdigit():
            sum_by_minutes = DEFAULT_TRACKER_SESSIONS_SUM_BY
    sum_by_minutes = int(sum_by_minutes)

    summed_tracker_sessions = sum_tracker_sessions(tracker_sessions, sum_by_minutes=sum_by_minutes)

    return render_template('get-tracker.html',
                           sum_seconds=sum_by_minutes * 60,
                           tracker=tracker,
                           summed_tracker_sessions=summed_tracker_sessions,
                           )


@client_bp.route('/trackers/create', methods=['POST'])
@login_required
def create_tracker():
    name = request.form.get('name')
    name = name.strip()
    if not name:
        flash("Error: Name can't be empty", category="error")
        return redirect(request.referrer)

    tracker_type = request.form.get('tracker_type')
    if tracker_type not in [t[0] for t in Tracker.TRACKER_TYPES]:
        flash("Error: Invalid tracker type.", category="error")
        return redirect(request.referrer)

    api_key = generate_random_string(12)
    tracker = Tracker.create(log_class=Log,
                             user=current_user,
                             tracker_type=tracker_type,
                             name=name,
                             api_key=api_key,
                             )
    db.session.add(tracker)
    db.session.commit()
    flash("Tracker created.")
    return redirect(request.referrer)


def verify_tracker_se(tracker_id, api_key):
    tracker_id = int(tracker_id)

    tracker = Tracker.query.filter_by(id=tracker_id).first()
    if not tracker:
        raise Exception("Non existing tracker with id {}.".format(tracker_id))

    if api_key != tracker.api_key:
        raise Exception("Incorrect API KEY.")
    return tracker


@client_bp.route('/tracker-sessions/create/batch', methods=['POST'])
@csrf.exempt
def create_tracker_sessions():
    tracker_id = request.json.get('tracker_id')
    api_key = request.json.get('api_key')
    try:
        tracker = verify_tracker_se(tracker_id, api_key)
    except Exception as e:
        return jsonify({"Error": "{}".format(e.message)}), 400

    sessions = request.json.get('sessions')
    if not sessions:
        return jsonify({"Error": "Sessions missing"}), 400

    user = tracker.user
    for ses in sessions:
        key = ses.get('key')
        active_time = ses.get('active_time', 0)
        total_time = ses.get('total_time', 0)
        try:
            meta = json.dumps(ses.get('meta', {}))
        except Exception as e:
            print("Problem parsing meta field: {}".format(e))
            print("meta: {}".format(ses.get('meta', {})))
            meta = u'{}'
        tracker_session = TrackerSession.create(key=key,
                                                active_time=active_time,
                                                total_time=total_time,
                                                user=user,
                                                tracker_id=tracker_id,
                                                meta=meta,
                                                )
        try:
            started_ts = ses.get('start_ts')
            tracker_session.started_ts = dt_parser.parse(started_ts)
        except Exception as e:
            return jsonify({"Error": "{}".format(e)}), 400
        try:
            finished_ts = ses.get('end_ts')
            finished_ts = dt_parser.parse(finished_ts)
        except Exception as e:
            return jsonify({"Error": "{}".format(e)}), 400
        if finished_ts < tracker_session.started_ts:
            return jsonify({"Error": "End time can't be before start time."}), 400
        tracker_session.finished_ts = finished_ts
        db.session.add(tracker_session)
    db.session.commit()
    return jsonify({
        "status": "{} Tracker Sessions created.".format(len(sessions)),
        "tracker_sessions": [s for s in sessions],
    })


@client_bp.route('/api/trackers/<int:tracker_id>/tracker-events')
@csrf.exempt
def api_get_tracker_events(tracker_id):
    api_key = request.args.get('api_key')
    try:
        tracker = verify_tracker_se(tracker_id, api_key)
    except Exception as e:
        return jsonify({"Error": "{}".format(e.message)}), 400

    tracker_type = request.args.get('tracker_type', '').lower()
    if tracker_type and tracker_type in [tt[0].lower() for tt in Tracker.TRACKER_TYPES]:
        filters = [Tracker.tracker_type == tracker_type, ]
    else:
        filters = [TrackerEvent.tracker == tracker, ]

    since = request.args.get('since')
    if since:
        try:
            parsed_ts = dt_parser.parse(since)
        except Exception:
            parsed_ts = 0
        filters.append(TrackerEvent.time > parsed_ts)

    client_not = request.args.get('client_not')
    if client_not:
        filters.append(Tracker.client != client_not)

    tracker_events = TrackerEvent.query.join(Tracker).filter(*filters).all()
    tracker_events_serialized = [
        tracker_event.serialize() for tracker_event in tracker_events
    ]
    return jsonify(dict(
        tracker_events=tracker_events_serialized
    ))


@client_bp.route('/tracker-events/create/batch', methods=['POST'])
@csrf.exempt
def create_tracker_events():
    api_key = request.json.get('api_key')
    tracker_id = request.json.get('tracker_id')
    try:
        tracker = verify_tracker_se(tracker_id, api_key)
    except Exception as e:
        return jsonify({"Error": "{}".format(e.message)}), 400

    events = request.json.get('events')
    if not events:
        return jsonify({"Error": "Events missing"}), 400

    user = tracker.user
    for event in events:
        key = event.get('key')
        time = event.get('time', 0)
        try:
            meta = json.dumps(event.get('meta', {}))
        except Exception as e:
            print("Problem parsing meta field: {}".format(e))
            print("meta: {}".format(event.get('meta', {})))
            meta = u'{}'
        tracker_event = TrackerEvent.create(key=key,
                                            time=time,
                                            user=user,
                                            meta=meta,
                                            tracker_id=tracker_id,
                                            )
        db.session.add(tracker_event)
    db.session.commit()
    return jsonify({
        "status": "{} Tracker Events created.".format(len(events)),
        "tracker_events": [s for s in events],
    })


@client_bp.route('/notes')
@login_required
def get_notes():
    try:
        ctx = Context().build_context()
    except Exception as e:
        flash(e.message, category="error")
        return redirect(url_for('client_bp.home'))
    notes = Note.query. \
        filter_by(user=current_user). \
        order_by(Note.created_ts.desc()).all()
    dated_notes = OrderedDefaultDict(list)
    for note in notes:
        dated_notes[note.created_ts.date()].append(note)
    return render_template('notes.html',
                           dated_notes=dated_notes,
                           ctx=ctx,
                           )


@client_bp.route('/notes/create', methods=['POST'])
@login_required
def create_note():
    body = request.form.get('body')
    note = Note.create(log_class=Log,
                       user=current_user,
                       body=body,
                       )
    db.session.add(note)
    db.session.commit()
    flash("Note created.")
    return redirect(request.referrer)


@client_bp.route('/notes/<note_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_note(note_id):
    note = Note.query.filter_by(id=note_id, user=current_user).first()
    if not note:
        flash("Note doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('edit-note.html', note=note)
    body = request.form.get('body')
    note.body = body
    db.session.add(note)
    db.session.commit()
    flash("Note edited.")
    return redirect(request.referrer)


@client_bp.route('/logs')
@login_required
def get_logs():
    try:
        ctx = Context().build_context()
    except Exception as e:
        flash(e.message, category="error")
        return redirect(url_for('client_bp.home'))
    log_type = request.args.get('log_type')
    if log_type == 'system':
        filters = dict(user=current_user, created_type=Log.CREATED_TYPE_AUTOMATIC)
    elif log_type == 'all':
        filters = dict(user=current_user)
    else:
        filters = dict(user=current_user, created_type=Log.CREATED_TYPE_MANUAL)

    logs = Log.query. \
        filter_by(**filters). \
        order_by(Log.created_ts.desc()).all()
    dated_logs = OrderedDefaultDict(list)
    for log in logs:
        dated_logs[log.created_ts.date()].append(log)
    return render_template('logs.html',
                           dated_logs=dated_logs,
                           ctx=ctx
                           )


@client_bp.route('/logs/create', methods=['POST'])
@login_required
def create_log():
    body = request.form.get('body')
    log = Log.create(log_class=Log,
                     user=current_user,
                     body=body,
                     )
    db.session.add(log)
    db.session.commit()
    flash("Log created.")
    return redirect(request.referrer)


@client_bp.route('/logs/<log_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_log(log_id):
    log = Log.query.filter_by(id=log_id, user=current_user).first()
    if not log:
        flash("Log doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('edit-log.html', log=log)
    body = request.form.get('body')
    log.body = body
    db.session.add(log)
    db.session.commit()
    flash("Log edited.")
    return redirect(request.referrer)


@client_bp.route('/search')
@login_required
def search():
    q = request.args.get('q')
    notes = Note.query. \
        whooshee_search(q). \
        filter_by(user=current_user).all()

    tasks = Task.query. \
        whooshee_search(q). \
        filter_by(user=current_user).all()

    calendar_events = CalendarEvent.query. \
        whooshee_search(q). \
        filter_by(user=current_user).all()

    activities = Activity.query. \
        whooshee_search(q). \
        filter_by(user=current_user).all()

    logs = Log.query. \
        whooshee_search(q). \
        filter_by(user=current_user).all()

    return render_template('search-results.html',
                           notes=notes,
                           tasks=tasks,
                           calendar_events=calendar_events,
                           activities=activities,
                           logs=logs,
                           q=q)
