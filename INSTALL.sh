#/bin/bash
echo "Provisioning sensitive file"
echo "======================================="
cp sensitive.py.sample sensitive.py

echo "Installing virtualenv"
echo "======================================="
sudo apt update
sudo apt install -y virtualenv

echo "Creating virtualenv"
echo "======================================="
virtualenv venv
source venv/bin/activate

echo "Installing python packages"
echo "======================================="
pip install -r requirements.txt

echo "Patching flask_markdown"
echo "======================================="
patch venv/lib/python2.7/site-packages/flaskext/markdown.py markdown.patch

echo "Populating db"
echo "======================================="
source venv/bin/activate && python populate.py
