var textarea = document.querySelector('textarea');

if (textarea)
  textarea.addEventListener('keydown', autosize);

function autosize() {
  var el = this;
  setTimeout(function () {
    el.style.cssText = 'height:auto; padding:0';
    el.style.cssText = 'height:' + (el.scrollHeight + 15) + 'px';
  }, 0);
}

function tickTimer(timer, display) {
  var hours, minutes, seconds;
  hours = timer / (60 * 60);
  minutes = parseInt(timer / 60 - parseInt(hours) * 60, 10);
  seconds = parseInt(timer % 60, 10);

  hours = hours === 0 ? "" : parseInt(hours) + ":";
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  display.textContent = hours + minutes + ":" + seconds;
}

function startTimer(timeThen, display) {
  var timeNow = new Date().getTime() / 1000;
  var timer = timeNow - timeThen;
  tickTimer(timer, display);
  timer++;
  setInterval(function () {
    tickTimer(timer, display);
    timer++;
  }, 1000);
}

function setInboxEntry(promptWord) {
  document.getElementById('inbox-entry').innerHTML = promptWord;
  document.getElementById('inbox-entry').focus();
  return false;
}

function iterateAll(className, cssText) {
  var elements = document.getElementsByClassName(className);
  for (var i = 0; i < elements.length; i++) {
    elements[i].style.cssText = cssText;
  }
}

function hideAll(className) {
  iterateAll(className, 'display: none;');
}

function showAll(className) {
  iterateAll(className, 'display: initial;');
}

function recurringTypeChange(obj) {
  console.log(obj.value);
  var value = obj.value;
  if (value === "NONE") {
    hideAll('repeating-hidden-default');
    hideAll('repeating-hidden-weekly');
  } else if (value === "WEEKLY") {
    showAll('repeating-hidden-weekly');
    showAll('repeating-hidden-default');
  } else {
    showAll('repeating-hidden-default');
    hideAll('repeating-hidden-weekly');
  }
}

function toggleTimeInputs(obj) {
  if (obj.checked) {
    hideAll('time-input');
  } else {
    showAll('time-input');
  }
}


window.onload = function () {
  var countUpTimerElements = document.getElementsByClassName('count-up-timer');
  for (var i = 0; i < countUpTimerElements.length; i++) {
    var element = countUpTimerElements[i];
    var timerValue = element.textContent;
    var timeThen = new Date(timerValue).getTime() / 1000;
    startTimer(timeThen, element);
  }

  hideAll('repeating-hidden-default');
  hideAll('repeating-hidden-weekly');
  hideAll('cal-default-hidden');
};