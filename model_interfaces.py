from copy import copy
from datetime import timedelta, datetime

from sqlalchemy import or_, and_
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy_utils import ChoiceType

from store import db
from utils import camel_case_to_snake_case


class ModelController(object):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def create(cls, log_class=None, **kwargs):
        now = datetime.utcnow()
        if issubclass(cls, Datable):
            kwargs['created_ts'] = now
        instance = cls(**kwargs)
        db.session.add(instance)
        if log_class:
            db.session.commit()
            log = log_class(body=u"[{}|{}|created] {}".format(instance.__class__.__name__, instance.id, repr(instance)),
                            created_type=log_class.CREATED_TYPE_AUTOMATIC,
                            created_ts=now,
                            )
            user = kwargs.get('user')
            if user:
                log.user = user
            db.session.add(log)
            db.session.commit()
        return instance

    @classmethod
    def get_by(cls, **kwargs):
        instance = cls.query.filter_by(**kwargs).first()
        if not instance:
            raise Exception("{} doesn't exist.".format(cls.__name__))
        return instance


class Ownable(object):
    @declared_attr
    def user_id(self):
        return db.Column(db.Integer, db.ForeignKey('user.id'))

    @declared_attr
    def user(self):
        return db.relationship("User")


class Datable(object):
    created_ts = db.Column(db.DateTime())
    modified_ts = db.Column(db.DateTime())
    deleted = db.Column(db.Boolean, default=False)
    deleted_ts = db.Column(db.DateTime())


class Schedulable(object):
    to_start_ts = db.Column(db.DateTime())
    to_finish_ts = db.Column(db.DateTime())

    all_day = db.Column(db.Boolean, default=False)

    @classmethod
    def get_filter_in_bounds(cls, start, end):
        return or_(
            # completely within start-end
            and_(cls.to_start_ts >= start,
                 cls.to_finish_ts <= end),
            or_(
                # starts before today, ends after today
                and_(cls.to_start_ts <= start,
                     cls.to_finish_ts >= end),
                or_(
                    # starts today
                    and_(cls.to_start_ts >= start,
                         cls.to_start_ts <= end),
                    # ends today
                    and_(cls.to_finish_ts <= end,
                         cls.to_finish_ts >= start))))


class Recurrable(Schedulable):
    RECURRING_NONE = u"NONE"
    RECURRING_DAILY = u"DAILY"
    RECURRING_WEEKLY = u"WEEKLY"
    RECURRING_MONTHLY = u"MONTHLY"
    RECURRING_YEARLY = u"YEARLY"

    RECURRING_TYPES = [
        (RECURRING_NONE, u'not repeating'),
        (RECURRING_DAILY, u'day'),
        (RECURRING_WEEKLY, u'week'),
        (RECURRING_MONTHLY, u'month'),
        (RECURRING_YEARLY, u'year'),
    ]

    RECURRING_ENDS_NEVER = u"NEVER"
    RECURRING_ENDS_AFTER_N = u"AFTER_N"
    RECURRING_ENDS_ON = u"ON"

    RECURRING_ENDS_TYPES = [
        (RECURRING_ENDS_NEVER, u'never'),
        (RECURRING_ENDS_AFTER_N, u'after'),
        (RECURRING_ENDS_ON, u'on'),
    ]

    recurring_to_start_ts = db.Column(db.DateTime())
    recurring_to_finish_ts = db.Column(db.DateTime())
    recurring_type = db.Column(ChoiceType(RECURRING_TYPES), default=RECURRING_NONE)  # NONE|DAILY|WEEKLY|MONTHLY|YEARLY
    recurring_every = db.Column(db.Integer, default=1)  # e.g. every 2nd day, 2nd week etc
    recurring_weekly_mask = db.Column(db.String)  # MTWTFSS -> 1111100(every week day)
    recurring_ends_type = db.Column(ChoiceType(RECURRING_ENDS_TYPES), default=RECURRING_ENDS_NEVER)  # NEVER|AFTER_N|ON
    recurring_ends_after_x = db.Column(db.Integer)
    recurring_ends_on = db.Column(db.DateTime())

    recurring_ended_on = db.Column(db.DateTime())  # when the scheduling was stopped
    recurring_off_on = db.Column(db.String)  # list of dates when the recurring was manually stopped

    @classmethod
    def filter_recurring(cls, recurring_events, start, end):
        # TODO: Handle recurring_every
        # TODO: Handle ENDS_AFTER_X
        # TODO: Handle recurring_ended_on, off_on
        erv = []
        start_of_start = start.replace(hour=0, minute=0, second=0, microsecond=0)
        end_of_end = (end + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
        delta_period = end_of_end - start_of_start
        days = []
        for d in range(delta_period.days):
            days.append(start_of_start + timedelta(days=d))
        for day in days:
            today_weekday = day.isoweekday()  # 1 is Monday, 7 is Sunday
            for event in recurring_events:
                should_add = False
                if event.recurring_type == cls.RECURRING_DAILY:
                    should_add = True
                elif event.recurring_type == cls.RECURRING_WEEKLY:
                    weekly_mask = event.recurring_weekly_mask
                    event_weekly_mask = weekly_mask[today_weekday - 1]
                    if event_weekly_mask == '1':
                        should_add = True
                elif event.recurring_type == cls.RECURRING_MONTHLY:
                    if event.recurring_to_start_ts.day == start.day:
                        should_add = True
                elif event.recurring_type == cls.RECURRING_YEARLY:
                    if event.recurring_to_start_ts.day == start.day \
                            and event.recurring_to_start_ts.month == event.recurring_to_start_ts == start.month:
                        should_add = True
                if should_add:
                    # replace event's start_ts with today's
                    event_copy = copy(event)
                    event_copy.to_start_ts = event.recurring_to_start_ts.replace(day=day.day,
                                                                                 month=day.month,
                                                                                 year=day.year)
                    delta = event_copy.recurring_to_finish_ts - event.recurring_to_start_ts
                    event_copy.to_finish_ts = event_copy.to_start_ts + delta
                    erv.append(event_copy)
        return list(set(erv))

    @classmethod
    def get_recurring_events_in_bounds(cls, user, start, end):
        recurring_events = cls.query.filter(
            and_(cls.recurring_type != cls.RECURRING_NONE,
                 and_(cls.user == user,
                      and_(cls.deleted == False,
                           or_(cls.recurring_ends_type == cls.RECURRING_ENDS_NEVER,
                               or_(cls.recurring_ends_type == cls.RECURRING_ENDS_AFTER_N,
                                   and_(
                                       cls.recurring_ends_type == cls.RECURRING_ENDS_ON,
                                       cls.recurring_ends_on >= start
                                   ))))))).all()
        recurring_events = cls.filter_recurring(recurring_events, start, end)
        return recurring_events

    @classmethod
    def get_normal_events_in_bounds(cls, user, start, end):
        regular_events = cls.query.filter(
            and_(cls.recurring_type == cls.RECURRING_NONE,
                 and_(cls.user == user,
                      and_(cls.deleted == False,  # don't use "is/is not" - it doesn't work
                           cls.get_filter_in_bounds(start, end))))).all()
        return regular_events

    @classmethod
    def get_events_in_bounds(cls, user, start, end):
        normal_events = cls.get_normal_events_in_bounds(user, start, end)
        recurring_events = cls.get_recurring_events_in_bounds(user, start, end)
        return list(set(normal_events + recurring_events))


class Runnable(object):
    started_ts = db.Column(db.DateTime())
    finished_ts = db.Column(db.DateTime())

    @classmethod
    def get_filter_in_bounds(cls, start, end):
        return or_(
            # completely within start-end
            and_(cls.started_ts >= start,
                 cls.finished_ts <= end),
            or_(
                # starts before today, ends after today
                and_(cls.started_ts <= start,
                     cls.finished_ts >= end),
                or_(
                    # starts today
                    and_(cls.started_ts >= start,
                         cls.started_ts <= end),
                    # ends today
                    and_(cls.finished_ts <= end,
                         cls.finished_ts >= start))))
