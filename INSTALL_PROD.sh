#/bin/bash
./INSTALL.sh
echo "Installing nginx"
echo "======================================="
sudo apt update
sudo apt install -y nginx

echo "Creating journal user"
echo "======================================="
sudo useradd journal_user

echo "Installing gunicorn service"
echo "======================================="
sudo cp gunicorn.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable gunicorn
sudo systemctl start gunicorn

echo "Generating letsencrypt certificate"
echo "======================================="
wget -O /tmp/certbot-auto https://dl.eff.org/certbot-auto
chmod +x /tmp/certbot-auto
sudo /tmp/certbot-auto --authenticator standalone --installer nginx --pre-hook "service nginx stop" --post-hook "service nginx start" --redirect --agree-tos --no-eff-email --email admin@app.com -d basic.prodday.com --no-bootstrap

echo "Copying over nginx config"
echo "======================================="
sudo rm /etc/nginx/sites-available/default.conf
sudo cp journal_nginx.conf /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/journal_nginx.conf /etc/nginx/sites-enabled/journal_nginx.conf
sudo systemctl restart nginx
