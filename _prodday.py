#!/usr/bin/env python
import argh
import sys

# TODO: if we go to completely SQLAlchemy approach without Flask
from models import InboxEntry
from auth_models import User

user = User.query.first()


def inbox():
    inbox_entries = InboxEntry.query.filter_by(user=user).all()
    for inbox_entry in inbox_entries:
        print inbox_entry.body


def task(name, greeting='Hello'):
    """Greets the user with given name. The greeting is customizable."""
    return greeting + ', ' + name


def event(text):
    """Returns given word as is."""
    return text


def activity(text):
    """Returns given word as is."""
    return text


def session(text):
    """Returns given word as is."""
    return text


parser = argh.ArghParser()
parser.add_commands([inbox, task, event, activity, session])

# dispatching:

if __name__ == '__main__':
    if len(sys.argv) == 1:
        inbox()
    else:
        parser.dispatch()
