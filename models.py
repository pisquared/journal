import json

from dateparser import parse as human_parse_dt
from datetime import datetime, timedelta
from sqlalchemy_utils import ChoiceType

from model_interfaces import ModelController, Ownable, Datable, Schedulable, Runnable, Recurrable
from search import whooshee
from store import db


@whooshee.register_model('body')
class InboxEntry(db.Model, ModelController, Ownable, Datable):
    body = db.Column(db.Unicode)

    is_processed = db.Column(db.Boolean, default=False)
    processed_type = db.Column(db.String)
    processed_instance_id = db.Column(db.Integer)


@whooshee.register_model('name')
class Project(db.Model, ModelController, Ownable, Datable):
    name = db.Column(db.Unicode)


@whooshee.register_model('name')
class Label(db.Model, ModelController, Ownable, Datable):
    name = db.Column(db.Unicode)


@whooshee.register_model('title', 'body')
class Task(db.Model, ModelController, Ownable, Datable, Runnable, Schedulable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    is_done = db.Column(db.Boolean, default=False)
    done_ts = db.Column(db.DateTime())

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", backref="tasks")


class TaskLabel(db.Model, ModelController, Ownable, Datable):
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    task = db.relationship("Task", backref="task_labels")

    label_id = db.Column(db.Integer, db.ForeignKey('label.id'))
    label = db.relationship("Label", backref="task_labels")


@whooshee.register_model('title', 'body')
class CalendarEvent(db.Model, ModelController, Ownable, Datable, Recurrable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)


@whooshee.register_model('body')
class Note(db.Model, ModelController, Ownable, Datable):
    body = db.Column(db.Unicode)


@whooshee.register_model('body')
class Log(db.Model, ModelController, Ownable, Datable):
    CREATED_TYPE_AUTOMATIC = u"AUTOMATIC"
    CREATED_TYPE_MANUAL = u"MANUAL"

    START_TYPES = [
        (CREATED_TYPE_MANUAL, 'manual'),
        (CREATED_TYPE_AUTOMATIC, 'automatic'),
    ]

    body = db.Column(db.Unicode)
    created_type = db.Column(ChoiceType(START_TYPES), default=CREATED_TYPE_MANUAL)


class ActivitySession(db.Model, ModelController, Ownable, Datable, Runnable):
    START_TYPE_AUTOMATIC = u"AUTOMATIC"
    START_TYPE_MANUAL = u"MANUAL"

    START_TYPES = [
        (START_TYPE_MANUAL, 'manual'),
        (START_TYPE_AUTOMATIC, 'automatic'),
    ]

    start_type = db.Column(ChoiceType(START_TYPES), default=START_TYPE_MANUAL)

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", foreign_keys=[activity_id], backref="activity_sessions")


@whooshee.register_model('name')
class Activity(db.Model, ModelController, Ownable, Datable):
    name = db.Column(db.Unicode)

    running_session_id = db.Column(db.Integer, db.ForeignKey('activity_session.id'))
    running_session = db.relationship("ActivitySession", foreign_keys=[running_session_id],
                                      backref="running_activity", uselist=False)

    def __repr__(self):
        return 'Activity {}: {}'.format(self.id, self.name)

    def __str__(self):
        return 'Activity {}: {}'.format(self.id, self.name)

    @property
    def is_running(self):
        return bool(self.running_session)

    @property
    def running_since_ts(self):
        if self.running_session:
            return self.running_session.started_ts

    def start(self, user, start_type=ActivitySession.START_TYPE_MANUAL):
        if self.running_session:
            raise Exception("Activity already running.")
        now = datetime.utcnow()
        running_session = ActivitySession.create(log_class=Log,
                                                 activity=self,
                                                 user=user,
                                                 start_type=start_type,
                                                 started_ts=now,
                                                 )
        db.session.add(self)
        db.session.commit()
        self.running_session = running_session
        db.session.add(running_session)

        log = Log.create(body=u"[Activity|{}|started|{}] {}".format(self.id, start_type, repr(self)),
                         user=user,
                         created_type=Log.CREATED_TYPE_AUTOMATIC,
                         )
        db.session.add(log)
        db.session.commit()

    def stop(self, user, stop_type=ActivitySession.START_TYPE_MANUAL):
        if not self.running_session:
            raise Exception("Activity not running.")
        now = datetime.utcnow()
        self.running_session.finished_ts = now
        db.session.add(self.running_session)
        db.session.commit()
        self.running_session = None
        db.session.add(self)

        log = Log.create(body=u"[Activity|{}|stopped|{}] {}".format(self.id, stop_type, repr(self)),
                         user=user,
                         created_type=Log.CREATED_TYPE_AUTOMATIC,
                         )
        db.session.add(log)
        db.session.commit()


class ActivityComponent(db.Model, ModelController, Ownable, Datable):
    tracker_type = db.Column(db.Unicode)
    search_key = db.Column(db.Unicode)

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", foreign_keys=[activity_id], backref="activity_components")


@whooshee.register_model('name', 'body')
class Goal(db.Model, ModelController, Datable, Ownable):
    PERIOD_DAY = u"DAY"
    PERIOD_WEEK = u"WEEK"
    PERIOD_FORTNIGHT = u"FORTNIGHT"
    PERIOD_MONTH = u"MONTH"

    LIMIT_MAX = u"MAX"
    LIMIT_MIN = u"MIN"

    PERIODS = [
        (PERIOD_DAY, u'day'),
        (PERIOD_WEEK, u'week'),
        (PERIOD_FORTNIGHT, u'fortnight'),
        (PERIOD_MONTH, u'month'),
    ]

    LIMITS = [
        (LIMIT_MIN, u'least'),
        (LIMIT_MAX, u'most'),
    ]

    name = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", backref="goal", uselist=False)

    frequency = db.Column(db.Integer, default=1)
    period = db.Column(ChoiceType(PERIODS))
    active_minutes = db.Column(db.Integer, default=10)
    limit = db.Column(ChoiceType(LIMITS))

    def __repr__(self):
        return '<Goal %r: %r %r>' % (self.id, self.name, self.user)


class Tracker(db.Model, ModelController, Datable, Ownable):
    TRACKER_TYPE_APPLICATION = u'APPLICATION'
    TRACKER_TYPE_DOMAIN = u'DOMAIN'
    TRACKER_TYPE_LOCATION = u'LOCATION'
    TRACKER_TYPE_SPORT_ACTIVITY = u'SPORT_ACTIVITY'
    TRACKER_TYPE_BASH_HISTORY = u'BASH_HISTORY'

    TRACKER_TYPES = [
        (TRACKER_TYPE_APPLICATION, 'Application Tracker'),
        (TRACKER_TYPE_DOMAIN, 'Domain Tracker'),
        (TRACKER_TYPE_LOCATION, 'Location Tracker'),
        (TRACKER_TYPE_SPORT_ACTIVITY, 'Sport Activity Tracker'),
        (TRACKER_TYPE_BASH_HISTORY, 'Bash History Tracker'),
    ]

    name = db.Column(db.Unicode)
    api_key = db.Column(db.Unicode)
    tracker_type = db.Column(ChoiceType(TRACKER_TYPES))

    client = db.Column(db.Unicode)


class TrackerSession(db.Model, ModelController, Ownable, Runnable):
    key = db.Column(db.Unicode)
    total_time = db.Column(db.Integer)
    active_time = db.Column(db.Integer)
    meta = db.Column(db.Unicode, default=u'{}')

    tracker_id = db.Column(db.Integer, db.ForeignKey('tracker.id'))
    tracker = db.relationship("Tracker", backref="tracker_sessions")

    def serialize(self):
        return {
            'key': self.key,
            'total_time': self.total_time,
            'active_time': self.active_time,
            'started_ts': self.started_ts,
            'finished_ts': self.finished_ts,
            'meta': json.loads(self.meta),
        }


class TrackerEvent(db.Model, ModelController, Ownable):
    key = db.Column(db.Unicode)
    time = db.Column(db.Integer)
    meta = db.Column(db.Unicode, default=u'{}')

    tracker_id = db.Column(db.Integer, db.ForeignKey('tracker.id'))
    tracker = db.relationship("Tracker", backref="tracker_events")

    def serialize(self):
        return {
            'key': self.key,
            'time': str(self.time),
            'client': self.tracker.client,
            'meta': json.loads(self.meta),
        }


class Settings(db.Model, ModelController, Ownable):
    inbox_entries_enabled = db.Column(db.Boolean, default=True)

    tasks_enabled = db.Column(db.Boolean, default=True)
    tasks_due_enabled = db.Column(db.Boolean, default=False)
    tasks_defer_enabled = db.Column(db.Boolean, default=False)
    tasks_labels_enabled = db.Column(db.Boolean, default=False)
    tasks_projects_enabled = db.Column(db.Boolean, default=False)

    calendar_events_enabled = db.Column(db.Boolean, default=False)
    calendar_events_recurring_enabled = db.Column(db.Boolean, default=False)

    activities_enabled = db.Column(db.Boolean, default=False)

    trackers_enabled = db.Column(db.Boolean, default=False)

    goals_enabled = db.Column(db.Boolean, default=False)

    notes_enabled = db.Column(db.Boolean, default=False)

    logs_enabled = db.Column(db.Boolean, default=False)


def calendar_event_update(calendar_event, title, to_start_ts, to_finish_ts):
    calendar_event.title = title
    calendar_event.to_start_ts = to_start_ts
    if to_finish_ts:
        if to_finish_ts < calendar_event.to_start_ts:
            raise Exception("End time can't be before start time.")
    else:
        to_finish_ts = to_start_ts + timedelta(hours=1)
    calendar_event.to_finish_ts = to_finish_ts

    now = datetime.utcnow()
    calendar_event.modified_ts = now
    db.session.add(calendar_event)
    db.session.commit()


def attempt_inbox_entry_process(body, user):
    now = datetime.utcnow()
    lined_body = body.split('\n')
    if len(lined_body) > 0:
        first_line_body = lined_body[0]
        first_line_body_lowered = first_line_body.lower().strip()

        splittable_words = ['todo:', 'task:']
        for splittable_word in splittable_words:
            if first_line_body_lowered.startswith(splittable_word):
                instance_title = splittable_word.join(first_line_body_lowered.split(splittable_word)[1:]).strip()
                task = Task(title=instance_title, user=user, created_ts=now)
                db.session.add(task)
                db.session.commit()
                return True, "task", task.id

        splittable_word = 'log:'
        if first_line_body_lowered.startswith(splittable_word):
            instance_body = splittable_word.join(first_line_body_lowered.split(splittable_word)[1:]).strip()
            log = Log(body=instance_body, user=user, created_ts=now)
            db.session.add(log)
            db.session.commit()
            return True, "log", log.id

        splittable_word = 'note:'
        if first_line_body_lowered.startswith(splittable_word):
            instance_body = splittable_word.join(first_line_body_lowered.split(splittable_word)[1:]).strip()
            note = Note(body=instance_body, user=user, created_ts=now)
            db.session.add(note)
            db.session.commit()
            return True, "note", note.id

        splittable_word = 'event:'
        if first_line_body_lowered.startswith(splittable_word):
            instance_title = splittable_word.join(first_line_body_lowered.split(splittable_word)[1:]).strip()
            to_start_ts, to_finish_ts = None, None
            if len(lined_body) > 1:
                for line in lined_body[1:3]:
                    line_lowered_stripped = line.lower().strip()
                    if line_lowered_stripped.startswith('start:'):
                        human_start_ts = 'start:'.join(line_lowered_stripped.split('start:')[1:]).strip()
                        to_start_ts = human_parse_dt(human_start_ts)
                    elif line_lowered_stripped.startswith('end:'):
                        human_end_ts = 'end:'.join(line_lowered_stripped.split('end:')[1:]).strip()
                        to_finish_ts = human_parse_dt(human_end_ts)
            if not to_start_ts:
                to_start_ts = datetime.utcnow().replace(microsecond=0, second=0, minute=0) + timedelta(hours=1)
            if not to_finish_ts:
                to_finish_ts = to_start_ts + timedelta(hours=1)
            if to_finish_ts < to_start_ts:
                to_finish_ts = to_start_ts + timedelta(hours=1)
            calendar_event = CalendarEvent(title=instance_title,
                                           to_start_ts=to_start_ts,
                                           to_finish_ts=to_finish_ts,
                                           user=user,
                                           created_ts=now)
            db.session.add(calendar_event)
            db.session.commit()
            return True, "calendar_event", calendar_event.id

        splittable_word = 'start:'
        if first_line_body_lowered.startswith(splittable_word):
            activity_name_hypothesis = ''.join(first_line_body_lowered.split(splittable_word)[1:]).strip()
            not_started_activities = Activity.query.filter(Activity.user == user,
                                                           Activity.is_running == False).all()
            # yes, this can be overriden by same named activities, but we are doing best effort here
            lowered_activities = {a.title.lower(): a for a in not_started_activities}
            if activity_name_hypothesis in lowered_activities:
                activity = lowered_activities[activity_name_hypothesis]
                activity.start(user=user)
                db.session.add(activity)
                db.session.commit()
                return True, "activity", activity.running_session.id
    return False, None, None


def activity_session_update(user, activity_session, activity_id, started_ts, finished_ts):
    activities = Activity.query.filter_by(user=user).all()
    activity_id = int(activity_id)
    if activity_id not in [a.id for a in activities]:
        raise Exception("Error: Non existing activity with id {}.".format(activity_id))
    activity_session.activity_id = activity_id
    activity_session.started_ts = started_ts
    if finished_ts:
        if finished_ts < activity_session.started_ts:
            raise Exception("End time can't be before start time.")
        activity_session.finished_ts = finished_ts
    db.session.add(activity_session)
    db.session.commit()
